package main

import (
	"log"
	"net"
	"io"
)

func main() {

	listener, err := net.Listen("tcp", ":8765")
	if err != nil {
		log.Fatal("Error while starting server", err)
	} else {
		log.Print("Server started on port 8765")
	}

	var connCounter int = 0
	for {
		conn, err := listener.Accept()
		connCounter++
		if err != nil {
			log.Print("Error while accepting connection", err)
		}
		go handleConnection(conn, connCounter)
	}
}

func handleConnection(conn net.Conn, id int) {

	for {
		request := make([]byte, 65535) //todo: how to clear content?
		_, errRead := conn.Read(request)
		if errRead != nil {
			if errRead == io.EOF {
				log.Printf("Connection %d was closed!", id)
				conn.Close()
				conn = nil
			} else {
				log.Printf("Error while reading from connection %d", id)
				log.Print(errRead)
			}
			return
		}
		_, errWrite := conn.Write(request)
		if errWrite != nil {
			if errWrite == io.EOF {
				log.Printf("Connection %d was closed!", id)
				conn.Close()
				conn = nil
			} else {
				log.Printf("Error in connection %d while writing response", id)
				log.Print(errWrite)
			}
			return
		}
	}
}
